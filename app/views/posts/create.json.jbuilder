json.id @post.id
json.title @post.title
json.description @post.description
json.owner @post.user.username
json.ownerId @post.user.id