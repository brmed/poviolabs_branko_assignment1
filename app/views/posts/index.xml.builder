xml.instruct!
xml.posts do
  @posts.each do |post|
    xml.post do
    	xml.id post.id
      xml.title post.title
      xml.description post.description
      xml.created_at post.created_at.strftime("%d.%m.%Y")
      xml.owner post.user.username
      xml.ownerId post.user.id
    end
  end
end