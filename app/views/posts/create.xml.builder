xml.instruct!
xml.post do
	xml.id @post.id
  xml.title @post.title
  xml.description @post.description
  xml.owner @post.user.username
  xml.ownerId @post.user.id
end