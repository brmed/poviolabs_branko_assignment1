json.array! @posts do |post|
	json.id post.id
	json.title post.title
	json.description post.description
	json.created_at post.created_at.strftime("%d.%m.%Y")
	json.owner post.user.username
	json.ownerId post.user.id
end