class User < ActiveRecord::Base
	VALID_EMAIL_REGEX = /\A([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  has_secure_password
  
  # Relations
  has_many :posts

  # Validations
  validates :username, presence: true, length: { minimum: 5, maximum: 12 }
  validates :password, presence: true, length: { minimum: 5 }
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }
end
