class PostsController < ApplicationController
  def index
  	@posts = Post.all.order(created_at: :desc)

  	respond_to do |format|
  		format.json
      format.xml
    end
  end

  def create
  	@post = current_user.posts.new post_params
  	if @post.save
  		respond_to do |format|
  			format.json
  			# format.xml
  		end
		else
			render status: :not_acceptable
		end
  end

  def destroy
    if current_user
      @post = Post.find_by id: params[:id]
      if @post.delete
        render json: @post, status: :ok
      end
    else
      render json: {error: "unauthorized" }, status: :unauthorized
    end
  end

  	private
  	def post_params
  		params.require(:post).permit(:title, :description)
  	end

end
