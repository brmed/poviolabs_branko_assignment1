blog.service('postService', ['Post', '$q', function(Post, $q){

	return {
		posts: Post.query(),

		newPost: function(post) {
			var defer = $q.defer();

			Post.save(post)
				.$promise.then(function(response){
					if (_.isObject(response)){
						defer.resolve(response);
					} else {
						defer.reject(response);
					}
				}, //error
					function(response) {
						defer.reject(response.data);
				});
			return defer.promise;
			},

		deletePost: function(post) {
			var defer = $q.defer();

			Post.delete({id: post.id})
				.$promise.then(function(response){
					if (_.isObject(response)){
						defer.resolve(response);
					} else {
						defer.reject(response);
					}
				}, //error
					function(response) {
						defer.reject(response);
				});
			return defer.promise;
		}
	}

}])