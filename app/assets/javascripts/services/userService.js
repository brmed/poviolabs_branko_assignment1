blog.service('userService', ['User', '$q', function(User, $q){

	// currentUser to be store in session or local or cookies
	this.currentUser = {};
	this.getUser = function(user) {
		self = this;
		var defer = $q.defer();

		User.findByEmail.get({email: user.email})
			.$promise.then(function(response){
				self.currentUser = response;
				defer.resolve(response);
			},
			// error response
			function(response){
				console.error(response);
				defer.reject(response.data);
			});

		return defer.promise;
	};

	this.createUser = function(user) {
		self = this;
		var defer = $q.defer();

		User.createUser.save({user: user})
			.$promise.then(function(response){
				self.currentUser = response;
				defer.resolve(response);
			},
			function(response){
				console.error(response);
				defer.reject(response);
			});

		return defer.promise;
	}
}])