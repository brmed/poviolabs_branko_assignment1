blog.config(['$urlRouterProvider', function($urlRouterProvider) {
  $urlRouterProvider.otherwise('/posts');
}])