blog.factory('User', ['$resource', function($resource){
	return {
		findByEmail: $resource('api/userbyemail'),
		createUser: $resource('api/users')
	}
}])