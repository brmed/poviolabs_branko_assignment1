blog.controller('topController', ['$scope', '$window', '$state', 'userService',
													function($scope, $window, $state, userService){

	$scope.$watch(function() { return userService.currentUser; },
		function(){
			$scope._currentUserFlag = !_.isEmpty(userService.currentUser);
			$scope._currentUser = userService.currentUser;
	});

	$scope.logOut = function(event){
		event.preventDefault();
		delete $window.sessionStorage.token;
		$state.go("posts");
		userService.currentUser = {};
		$.growl.notice({title: "User", message: "successfully sign out"});
	}
}])