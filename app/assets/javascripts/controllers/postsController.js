blog.controller('postsController', ['$scope', '$state', '$interval', 'postService', 'userService',
														function($scope, $state, $interval, postService, userService){
	$scope.posts = postService.posts;
	$scope.deletePost = {};
	$scope.featurePostFlag = false;
	var tmpFeaturePost = {};
	var featurePostInterval

	$scope.$watch(function() { return userService.currentUser; },
			function(){
				$scope._currentUser = userService.currentUser;
	});

	featurePostInterval = $interval(function(){
		if (_.isEmpty(tmpFeaturePost)){
			$scope.featurePost = _.sample($scope.posts);
		} else {
			$scope.featurePost = _.sample(_.without($scope.posts,tmpFeaturePost));
		}
		tmpFeaturePost = $scope.featurePost;
		$scope.featurePostFlag = true;
	}, 300000);

	$scope.$on('$destroy', function() {
	// Make sure that the interval is destroyed too
		if (angular.isDefined(featurePostInterval)) {
      $interval.cancel(featurePostInterval);
      featurePostInterval = undefined;
      $scope.featurePostFlag = false;
    }
	});

	$scope.createNewPost = function(){
		if (!_.isEmpty($scope._currentUser)) {
			postService.newPost($scope.newPost)
				.then(function(data){
					postService.posts.unshift(data);
					$scope.newPost = {};
					$state.go('posts');
					$.growl.notice({title: 'Post', message: "is successfully created"});
				},
					function(errorResponce){
						console.error(errorResponce);
				});
		};
	};

	$scope.openPopUp = function(event, post){
		event.preventDefault();
		$scope.deletePost = post;
		$('.delete-post').modal('show');
	};

	$scope.delPost = function(){
		postService.deletePost($scope.deletePost)
			.then(function(response){
				index = _.indexOf(postService.posts, $scope.deletePost);
				postService.posts.splice(index, 1);
				$('.delete-post').modal('hide');
				$state.go('posts');
				$.growl.notice({title: 'Post', message: "is successfully deleted"});
			},
				function(error){
					console.error(error.status);
					if (error.status === 401){
						$.growl.error({title: 'Unauthorized', message: "post is not deleted"});
						$('.delete-post').modal('hide');
					}
				})
	};

}])