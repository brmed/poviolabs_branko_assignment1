blog.controller('userController', ['$scope', '$http', '$state', '$window', 'userService',
													function($scope, $http, $state, $window, userService){

	$scope.logIn = function(event, user) {
		event.preventDefault();

		$http
			.post('knock/auth_token', {auth: user})
			.success(function(data, status, headers, config){
				$window.sessionStorage.token = data.jwt;
				// $window.localStorage.token = data.jwt;

				userService.getUser(user)
					.then(function(response){
						$state.go("posts");
						$.growl.notice({title: "User", message: "is logged in"});
					}, function(error){
						console.error(error);
						$.growl.error({message: "Something went wrong"});
					});

			})
			.error(function(data, status, headers, config){
				delete $window.sessionStorage.token;
				$.growl.error({message: "Invalid email or password!"});
			});
	};

	$scope.signUp = function(event){
		event.preventDefault();

		userService.createUser($scope.newUser)
			.then(function(response) {
				$.growl.notice({title: "User", message: "successfully created"});
				$scope.logIn(event, $scope.newUser);
				$state.go("posts");
			}, function(error){
				console.error(error);
				$.growl.error({message: "Something went wrong"});
			});
	}

}])