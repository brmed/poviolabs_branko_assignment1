Rails.application.routes.draw do
  root 'blog#index'

  mount Knock::Engine => "/knock"

  scope :api, defaults: {format: :json} do
    # resources :posts, only: [:index, :create, :destroy]
    resources :posts
    resources :users, only: :create
    get 'userbyemail', to: 'users#userbyemail'
  end

end
